import Vue from "vue";
import VueRouter from "vue-router";

import Login from "./pages/Auth/Login";
import ConfirmEmail from "./pages/Auth/ConfirmEmail";
import ResetPassword from "./pages/Auth/ResetPassword";
import ForgotPassword from "./pages/Auth/ForgotPassword";
import Dashboard from "./pages/Dashboard";
import Customer from "./pages/Customer";
import Support from "./pages/Support";
import CustomerList from "./pages/Customer/List";
import CreateCustomer from "./pages/Customer/Create";
import CustomerTickets from "./pages/Customer/Tickets";
import MyTickets from "./pages/Customer/MyTickets";
import MyTicketClosed from "./pages/Customer/ClosedTickets";
import Thanku from "./pages/Customer/Thanku";
import MyMessage from "./pages/Customer/MyMessage";
import NewTicket from "./pages/Customer/NewTickets";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      redirect: "/auth/login",
    },
    {
      path: "/auth/login",
      name: "Login",
      component: Login,
    },
    {
      path: "/auth/confirm-email",
      name: "Confirm Email",
      component: ConfirmEmail,
    },
    {
      path: "/auth/forgot-password",
      name: "Forgot Password",
      component: ForgotPassword,
    },
    {
      path: "/auth/reset-password",
      name: "Reset Password",
      component: ResetPassword,
    },
    {
      path: "/dashboard",
      name: "Dashboard",
      component: Dashboard,
    },
    {
      path: "/customer",
      name: "Customer",
      component: Customer,
    },
    {
      path: "/support",
      name: "Support",
      component: Support,
    },
    {
      path: "/customer/list",
      name: "Customer List",
      component: CustomerList,
    },
    {
      path: "/customer/create",
      name: "Create Customer",
      component: CreateCustomer,
    },
    {
      path: "/customer/tickets",
      name: "Customer Tickets",
      component: CustomerTickets,
    },
    {
      path: "/customer/mytickets",
      name: "My Tickets",
      component: MyTickets,
    },
    {
      path: "/customer/myticketsclosed",
      name: "My Closed Tickets",
      component: MyTicketClosed,
    },
    {
      path: "/customer/thanku",
      name: "Thank You",
      component: Thanku,
    },
    {
      // MyMessage
      path: "/customer/mymessage",
      name: "My Message",
      component: MyMessage,
    },
    {
      // MyMessage
      path: "/customer/new",
      name: "New Ticket",
      component: NewTicket,
    }
   
  ],
});

export default router;
