import axios from 'axios'

// const access_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE2MzgwNjQzMjksImlzcyI6Imh0dHBzOlwvXC9zdGcuZWRpdC5jby5pbFwvYXNvY3MtZmVcLyIsIm5iZiI6MTYzODA2NDMyOSwiZXhwIjoxNjQwNjU2MzI5LCJ0eXBlIjoiYXV0aCIsInVzZXJOYW1lIjoiZHJvcmVAYXNvY3NjbG91ZC5jb20iLCJjdXN0TmFtZSI6IjEwMDAyNCIsInJvbGVUeXBlIjoiVCIsInVzZXJJZCI6MzQ2fQ.jfZrur5QAJtoIOawstIOtrE-QX_I8A-ohPt0iftd1_JiG__pyW9F5GWNf5RCJjXMK2T2OrmnPme2xST9JVC2JA';
const apiClient = axios.create({
    // baseURL: 'https://stg.edit.co.il/asocs-be-wp',
    baseURL: 'https://portal-be.asocscloud.com',
    // withCredentials:false,
    headers:{
        Accept:'application/json',
        'Content-Type': 'application/json',
        // Authorization: `Bearer ${access_token}`
    },
    
})
 export default {
     // login
     login(obj){
         return apiClient.post('/user/login', obj);
     },
     // Get all projects
     getProjects(){
         return apiClient.get('/projects');
     },
     // get parts
     getParts(v){
         return apiClient.get(`parts/${v}`);
     },
     // Get all closed tickets
     getClosedTicktet(){
         return apiClient.get('/support/tickets/closed')
     }
 }
