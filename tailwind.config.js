module.exports = {
  mode: "jit",
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  darkMode: false,
  theme: {
    extend: {
      borderRadius: {
        "4xl": "2rem",
        "5xl": "2.5rem",
      },
      boxShadow: {
        card: "0 0 15px #00000025",
        navCard: "0 0 15px #6B8F3E",
        selectbox: "0 0 6px #00000025",
        tabletr: "1px 1px 0px #7FCC30, -1px -1px #7FCC30, 0 0 15px #00000025",
      },
      colors: {
        success: "#7FCC30",
        dark: "#06152B",
        secondary: "#303645",
        
        gray: "#B4B4B4",
        primary: "#3A36DB",
        pink: "#C389FF",
        danger: "#E54F4F",
      },
      fontFamily: {
        lato: ["Lato"],
        arial: ["Arial"],
      },
      fontSize: {
        xxs: "10px",
      },
      width: {

        '1/10': '10%',
        '2/10': '20%',
        '3/10': '30%',
        '4/10': '40%',
        '5/10': '50%',
        '6/10': '60%',
        '7/10': '70%',
        '8/10': '80%',
        '9/10': '90%',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
